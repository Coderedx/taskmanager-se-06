package com.shumov.tm.util.constant;

public enum UserRoleType {

    ADMIN("Administrator"),
    USER("User");

    String displayName;

    UserRoleType(String displayName){
        this.displayName = displayName;
    }

    @Override
    public String toString() {
        return "displayName= " + displayName;
    }
}
