package com.shumov.tm.api.service;

import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.User;
import com.shumov.tm.service.ProjectService;
import com.shumov.tm.service.TaskService;
import com.shumov.tm.service.UserService;

import java.util.List;
import java.util.Scanner;

public interface ServiceLocator {

    TaskService getTaskService();
    ProjectService getProjectService();
    Scanner getTerminalService();
    List<AbstractCommand> getCommands();
    User getCurrentUser();
    UserService getUserService();
    void setCurrentUser(User currentUser);
}
