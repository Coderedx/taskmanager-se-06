package com.shumov.tm.command.help;

import com.shumov.tm.command.AbstractCommand;

public class ExitCommand extends AbstractCommand {

    public ExitCommand() {

    }

    @Override
    public String command() {
        return "exit";
    }

    @Override
    public String getDescription() {
        return "Exit";
    }

    @Override
    public void execute() throws Exception {
        System.exit(0);
    }

    public boolean isSecure() {
        return true;
    }

}
