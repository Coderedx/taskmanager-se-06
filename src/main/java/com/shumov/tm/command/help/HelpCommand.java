package com.shumov.tm.command.help;

import com.shumov.tm.command.AbstractCommand;

public final class HelpCommand extends AbstractCommand {

    public HelpCommand() {

    }

    @Override
    public String command() {
        return "help";
    }

    @Override
    public String getDescription() {
        return "Show all commands";
    }

    @Override
    public void execute() throws Exception {
        for (final AbstractCommand command : serviceLocator.getCommands()){
            if(command.isSecure()){
                System.out.println(command.command()+": "+command.getDescription());
            }
        }
    }

    @Override
    public boolean isSecure() {
        return true;
    }
}
