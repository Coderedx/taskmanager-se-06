package com.shumov.tm.command.entity.task;

import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.User;
import com.shumov.tm.service.TaskService;
import com.shumov.tm.util.constant.UserRoleType;

public class TaskEditCommand extends AbstractCommand {

    public TaskEditCommand() {

    }

    @Override
    public String command() {
        return "task-edit";
    }

    @Override
    public String getDescription() {
        return "Edit task name";
    }

    @Override
    public void execute() throws Exception {
        if(serviceLocator.getCurrentUser().getUserRoleType().equals(UserRoleType.ADMIN)){
            adminExecute();
        } else {
            userExecute();
        }
    }

    public void adminExecute() throws Exception {
        System.out.println("[EDIT TASK NAME BY ID]");
        System.out.println("ENTER TASK ID:");
        String taskIdEdit = serviceLocator.getTerminalService().nextLine();
        TaskService taskService = serviceLocator.getTaskService();
        taskService.isWrongTaskId(taskIdEdit);
        System.out.println("ENTER NEW TASK NAME:");
        String taskNameEdit = serviceLocator.getTerminalService().nextLine();
        taskService.isWrongTaskName(taskNameEdit);
        taskService.editTaskNameById(taskIdEdit,taskNameEdit);
        System.out.println("TASK NAME HAS BEEN CHANGED SUCCESSFULLY");
    }

    public void userExecute() throws Exception {
        System.out.println("[EDIT TASK NAME BY ID]");
        System.out.println("ENTER TASK ID:");
        String taskIdEdit = serviceLocator.getTerminalService().nextLine();
        TaskService taskService = serviceLocator.getTaskService();
        taskService.isWrongTaskId(taskIdEdit);
        System.out.println("ENTER NEW TASK NAME:");
        String taskNameEdit = serviceLocator.getTerminalService().nextLine();
        taskService.isWrongTaskName(taskNameEdit);
        User user = serviceLocator.getCurrentUser();
        taskService.editTaskNameById(taskIdEdit,taskNameEdit, user.getId());
        System.out.println("TASK NAME HAS BEEN CHANGED SUCCESSFULLY");
    }
}

