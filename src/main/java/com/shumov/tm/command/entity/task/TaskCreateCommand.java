package com.shumov.tm.command.entity.task;

import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.Project;
import com.shumov.tm.entity.User;
import com.shumov.tm.service.ProjectService;
import com.shumov.tm.service.TaskService;

public class TaskCreateCommand extends AbstractCommand {

    public TaskCreateCommand() {

    }

    @Override
    public String command() {
        return "task-create";
    }

    @Override
    public String getDescription() {
        return "Create new task";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK CREATE]\nENTER TASK NAME:");
        String taskNameCreate = serviceLocator.getTerminalService().nextLine();
        TaskService taskService = serviceLocator.getTaskService();
        taskService.isWrongTaskName(taskNameCreate);
        System.out.println("ENTER PROJECT ID:");
        String projectId = serviceLocator.getTerminalService().nextLine();
        ProjectService projectService = serviceLocator.getProjectService();
        projectService.isWrongProjectId(projectId);
        Project project = projectService.getProject(projectId);
        User user = serviceLocator.getCurrentUser();
        taskService.createTask(taskNameCreate, project.getId(), user.getId());
        System.out.println("TASK HAS BEEN CREATED SUCCESSFULLY");
    }
}
