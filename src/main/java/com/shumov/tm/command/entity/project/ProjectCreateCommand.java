package com.shumov.tm.command.entity.project;

import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.User;
import com.shumov.tm.service.ProjectService;

public class ProjectCreateCommand extends AbstractCommand {

    public ProjectCreateCommand() {

    }

    @Override
    public String command() {
        return "project-create";
    }

    @Override
    public String getDescription() {
        return "Create new project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT CREATE]\nENTER PROJECT NAME:");
        String projectNameCreate = serviceLocator.getTerminalService().nextLine();
        ProjectService projectService = serviceLocator.getProjectService();
        projectService.isWrongProjectName(projectNameCreate);
        User user = serviceLocator.getCurrentUser();
        projectService.createProject(projectNameCreate, user.getId());
        System.out.println("PROJECT HAS BEEN CREATED SUCCESSFULLY");
    }
}
