package com.shumov.tm.command.entity.project;

import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.User;
import com.shumov.tm.service.ProjectService;
import com.shumov.tm.service.TaskService;
import com.shumov.tm.util.constant.UserRoleType;

public class ProjectRemoveCommand extends AbstractCommand {

    public ProjectRemoveCommand() {

    }

    @Override
    public String command() {
        return "project-remove";
    }

    @Override
    public String getDescription() {
        return "Remove selected project";
    }

    @Override
    public void execute() throws Exception {
        if(serviceLocator.getCurrentUser().getUserRoleType().equals(UserRoleType.ADMIN)){
            adminExecute();
        } else {
            userExecute();
        }
    }

    public void adminExecute() throws Exception {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("ENTER PROJECT ID:");
        String projectIdRemove = serviceLocator.getTerminalService().nextLine();
        ProjectService projectService = serviceLocator.getProjectService();
        TaskService taskService = serviceLocator.getTaskService();
        projectService.isWrongProjectId(projectIdRemove);
        projectService.removeProjectById(taskService, projectIdRemove);
        System.out.println("PROJECT HAS BEEN REMOVED SUCCESSFULLY");
    }

    public void userExecute() throws Exception {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("ENTER PROJECT ID:");
        String projectIdRemove = serviceLocator.getTerminalService().nextLine();
        ProjectService projectService = serviceLocator.getProjectService();
        TaskService taskService = serviceLocator.getTaskService();
        projectService.isWrongProjectId(projectIdRemove);
        User user = serviceLocator.getCurrentUser();
        projectService.removeProjectById(taskService, projectIdRemove, user.getId());
        System.out.println("PROJECT HAS BEEN REMOVED SUCCESSFULLY");
    }
}
