package com.shumov.tm.command.entity.task;

import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.User;
import com.shumov.tm.service.TaskService;
import com.shumov.tm.util.constant.UserRoleType;

public class TaskRemoveCommand extends AbstractCommand {

    public TaskRemoveCommand() {

    }

    @Override
    public String command() {
        return "task-remove";
    }

    @Override
    public String getDescription() {
        return "Remove selected task";
    }

    @Override
    public void execute() throws Exception {
        if(serviceLocator.getCurrentUser().getUserRoleType().equals(UserRoleType.ADMIN)){
            adminExecute();
        } else {
            userExecute();
        }
    }

    public void adminExecute() throws Exception {
        System.out.println("[REMOVE TASK BY ID]");
        System.out.println("ENTER TASK ID:");
        String taskIdRemove = serviceLocator.getTerminalService().nextLine();
        TaskService taskService = serviceLocator.getTaskService();
        taskService.isWrongTaskId(taskIdRemove);
        taskService.removeTaskById(taskIdRemove);
        System.out.println("TASK HAS BEEN REMOVED SUCCESSFULLY");
    }

    public void userExecute() throws Exception {
        System.out.println("[REMOVE TASK BY ID]");
        System.out.println("ENTER TASK ID:");
        String taskIdRemove = serviceLocator.getTerminalService().nextLine();
        TaskService taskService = serviceLocator.getTaskService();
        taskService.isWrongTaskId(taskIdRemove);
        User user = serviceLocator.getCurrentUser();
        taskService.removeTaskById(taskIdRemove,user.getId());
        System.out.println("TASK HAS BEEN REMOVED SUCCESSFULLY");
    }
}
