package com.shumov.tm.command.entity.project;

import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.Project;
import com.shumov.tm.entity.User;
import com.shumov.tm.util.constant.UserRoleType;

public class ProjectListCommand extends AbstractCommand {

    public ProjectListCommand() {

    }

    @Override
    public String command() {
        return "project-list";
    }

    @Override
    public String getDescription() {
        return "Show all projects";
    }

    @Override
    public void execute() throws Exception {
       if(serviceLocator.getCurrentUser().getUserRoleType().equals(UserRoleType.ADMIN)){
           adminExecute();
       } else {
           userExecute();
       }
    }

    public void adminExecute() throws Exception {
        System.out.println("[PROJECT LIST]");
        for (Project project : serviceLocator.getProjectService().getProjectList()) {
            System.out.println("PROJECT ID: " + project.getId() +"\n"
                    + "PROJECT NAME: " + project.getName() + "\n" +
                    "USER ID: " + project.getUserId()+ "\n");
        }
    }

    public void userExecute() throws Exception {
        System.out.println("[PROJECT LIST]");
        User user = serviceLocator.getCurrentUser();
        for (Project project : serviceLocator.getProjectService().getProjectList(user.getId())) {
            System.out.println("PROJECT ID: " + project.getId() + " PROJECT NAME: " + project.getName());
        }
    }
}
