package com.shumov.tm.command.entity.task;

import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.Task;
import com.shumov.tm.entity.User;
import com.shumov.tm.service.ProjectService;
import com.shumov.tm.service.TaskService;
import com.shumov.tm.util.constant.UserRoleType;

public class TaskAddProjectCommand extends AbstractCommand {

    public TaskAddProjectCommand() {

    }

    @Override
    public String command() {
        return "task-add";
    }

    @Override
    public String getDescription() {
        return "Add task in project";
    }

    @Override
    public void execute() throws Exception {
        if(serviceLocator.getCurrentUser().getUserRoleType().equals(UserRoleType.ADMIN)){
            adminExecute();
        } else {
            userExecute();
        }
    }

    public void adminExecute() throws Exception {
        System.out.println("[ADD TASK IN PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        String projectIdAdd = serviceLocator.getTerminalService().nextLine();
        ProjectService projectService = serviceLocator.getProjectService();
        projectService.isWrongProjectId(projectIdAdd);
        projectService.getProject(projectIdAdd);
        System.out.println("ENTER TASK ID:");
        String taskIdAdd = serviceLocator.getTerminalService().nextLine();
        TaskService taskService = serviceLocator.getTaskService();
        taskService.isWrongTaskId(taskIdAdd);
        Task task = taskService.getTask(taskIdAdd);
        task.setIdProject(projectIdAdd);
        taskService.addTaskInProject(taskIdAdd,task);
        System.out.println("TASK HAS BEEN ADD SUCCESSFULLY");
    }

    public void userExecute() throws Exception {
        System.out.println("[ADD TASK IN PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        String projectIdAdd = serviceLocator.getTerminalService().nextLine();
        ProjectService projectService = serviceLocator.getProjectService();
        projectService.isWrongProjectId(projectIdAdd);
        User user = serviceLocator.getCurrentUser();
        projectService.getProject(projectIdAdd, user.getId());
        System.out.println("ENTER TASK ID:");
        String taskIdAdd = serviceLocator.getTerminalService().nextLine();
        TaskService taskService = serviceLocator.getTaskService();
        taskService.isWrongTaskId(taskIdAdd);
        Task task = taskService.getTask(taskIdAdd,user.getId());
        task.setIdProject(projectIdAdd);
        taskService.addTaskInProject(taskIdAdd,task, user.getId());
        System.out.println("TASK HAS BEEN ADD SUCCESSFULLY");
    }
}
