package com.shumov.tm.command.entity.user;

import com.shumov.tm.api.service.ServiceLocator;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.User;
import com.shumov.tm.service.UserService;
import com.shumov.tm.util.HashMd5;

import java.util.Scanner;

public class UserLoginCommand extends AbstractCommand {

    @Override
    public String command() {
        return "login";
    }

    @Override
    public String getDescription() {
        return "User login";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[USER LOGIN]");
        System.out.println("ENTER LOGIN:");
        Scanner terminalService = serviceLocator.getTerminalService();
        String login = terminalService.nextLine();
        UserService userService = serviceLocator.getUserService();
        userService.isWrongLogin(login);
        System.out.println("ENTER PASSWORD:");
        String pass = terminalService.nextLine();
        userService.isWrongPass(pass);
        User user = userService.getUser(login, HashMd5.getMd5(pass));
        System.out.println("authorization successful".toUpperCase());
        serviceLocator.setCurrentUser(user);
    }

    @Override
    public boolean isSecure() {
        if(serviceLocator.getCurrentUser()==null){
            return true;
        } else {
            return false;
        }
    }
}
