package com.shumov.tm.command.entity.task;

import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.Task;
import com.shumov.tm.entity.User;
import com.shumov.tm.util.constant.UserRoleType;

public class TaskListCommand extends AbstractCommand {

    public TaskListCommand() {

    }

    @Override
    public String command() {
        return "task-list";
    }

    @Override
    public String getDescription() {
        return "Show all tasks";
    }

    @Override
    public void execute() throws Exception {
        if(serviceLocator.getCurrentUser().getUserRoleType().equals(UserRoleType.ADMIN)){
            adminExecute();
        } else {
            userExecute();
        }
    }

    public void userExecute() throws Exception {
        System.out.println("[TASK LIST]");
        User user = serviceLocator.getCurrentUser();
        for (Task task : serviceLocator.getTaskService().getTaskList(user.getId())) {
            System.out.println("TASK ID: " + task.getId() + " TASK NAME: " + task.getName()
                    + " PROJECT ID: "+ task.getIdProject());
        }
    }

    public void adminExecute() throws Exception {
        System.out.println("[TASK LIST]");
        for (Task task : serviceLocator.getTaskService().getTaskList()) {
            System.out.println("TASK ID: " + task.getId() + "\nTASK NAME: " + task.getName()
                    + "\nPROJECT ID: "+ task.getIdProject()
                    + "\nUSER ID: "+ task.getUserId()+"\n");
        }
    }

}
