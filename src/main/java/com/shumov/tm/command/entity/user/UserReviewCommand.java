package com.shumov.tm.command.entity.user;

import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.User;
import com.shumov.tm.util.constant.UserRoleType;

public class UserReviewCommand extends AbstractCommand {

    @Override
    public String command() {
        return "user-review";
    }

    @Override
    public String getDescription() {
        return "User review";
    }

    @Override
    public void execute() throws Exception {
        User user = serviceLocator.getCurrentUser();
        if(user.getUserRoleType().equals(UserRoleType.ADMIN)){
            reviewAdmin();
        } else if (user.getUserRoleType().equals(UserRoleType.USER)){
            reviewUser();
        }
    }

    private void reviewAdmin() throws Exception {
        System.out.println("[USER REVIEW]");
        User user = serviceLocator.getCurrentUser();
        System.out.println("Login: "+ user.getLogin());
        System.out.println("ID: "+ user.getId());
        System.out.println("Description: "+ user.getDescription() +"\n");
    }

    private void reviewUser() throws Exception {
        System.out.println("[USER REVIEW]");
        User user = serviceLocator.getCurrentUser();
        System.out.println("Login: "+ user.getLogin());
        System.out.println("Description: "+ user.getDescription() +"\n");
    }
}
