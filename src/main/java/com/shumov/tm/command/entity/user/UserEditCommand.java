package com.shumov.tm.command.entity.user;

import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.User;
import com.shumov.tm.service.UserService;

import java.util.Scanner;

public class UserEditCommand extends AbstractCommand {
    @Override
    public String command() {
        return "user-edit";
    }

    @Override
    public String getDescription() {
        return "Edit user description";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[EDIT USER DESCRIPTION]");
        System.out.println("ENTER NEW DESCRIPTION:");
        Scanner terminalService = serviceLocator.getTerminalService();
        String description = terminalService.nextLine();
        UserService userService = serviceLocator.getUserService();
        userService.isWrongDecrip(description);
        User user = serviceLocator.getCurrentUser();
        user.setDescription(description);
        userService.mergeUser(user);
        System.out.println("Description changed successfully".toUpperCase());
    }
}
