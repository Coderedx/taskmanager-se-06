package com.shumov.tm.command.entity.project;

import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.Task;
import com.shumov.tm.entity.User;
import com.shumov.tm.service.ProjectService;
import com.shumov.tm.util.constant.UserRoleType;

public class ProjectTasksCommand extends AbstractCommand {

    public ProjectTasksCommand() {

    }

    @Override
    public String command() {
        return "project-tasks";
    }

    @Override
    public String getDescription() {
        return "Review project tasks";
    }

    @Override
    public void execute() throws Exception {
        if(serviceLocator.getCurrentUser().getUserRoleType().equals(UserRoleType.ADMIN)){
            adminExecute();
        } else {
            userExecute();
        }
    }

    public void adminExecute() throws Exception {
        System.out.println("[REVIEW PROJECT TASKS]");
        System.out.println("ENTER PROJECT ID:");
        String projectId = serviceLocator.getTerminalService().nextLine();
        ProjectService projectService = serviceLocator.getProjectService();
        projectService.isWrongProjectId(projectId);
        projectService.getProject(projectId);
        System.out.println("[TASK LIST]");
        for (Task task : serviceLocator.getTaskService().getProjectTasks(projectId)) {
            System.out.println("TASK ID: " + task.getId() + "\nTASK NAME: " + task.getName()+
                    "\nUSER ID: "+ task.getUserId()+"\n");
        }
    }

    public void userExecute() throws Exception {
        System.out.println("[REVIEW PROJECT TASKS]");
        System.out.println("ENTER PROJECT ID:");
        String projectId = serviceLocator.getTerminalService().nextLine();
        ProjectService projectService = serviceLocator.getProjectService();
        projectService.isWrongProjectId(projectId);
        User user = serviceLocator.getCurrentUser();
        projectService.getProject(projectId, user.getId());
        System.out.println("[TASK LIST]");
        for (Task task : serviceLocator.getTaskService().getProjectTasks(projectId, user.getId())) {
            System.out.println("TASK ID: " + task.getId() + " TASK NAME: " + task.getName());
        }
    }
}
