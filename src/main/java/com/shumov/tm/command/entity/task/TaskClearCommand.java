package com.shumov.tm.command.entity.task;

import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.User;
import com.shumov.tm.util.constant.UserRoleType;

public class TaskClearCommand extends AbstractCommand {

    public TaskClearCommand() {

    }

    @Override
    public String command() {
        return "task-clear";
    }

    @Override
    public String getDescription() {
        return "Remove all tasks";
    }

    @Override
    public void execute() throws Exception {
        if(serviceLocator.getCurrentUser().getUserRoleType().equals(UserRoleType.ADMIN)){
            adminExecute();
        } else {
            userExecute();
        }
    }

    public void userExecute() throws Exception {
        System.out.println("[CLEAR TASKS]");
        System.out.println("[Enter \"Y\" if you confirm deletion of all tasks]".toUpperCase());
        String confirm = serviceLocator.getTerminalService().nextLine();
        if(!"y".equals(confirm.toLowerCase())){
            System.out.println("[operation has not been confirmed]".toUpperCase());
            return;
        }
        User user = serviceLocator.getCurrentUser();
        serviceLocator.getTaskService().clearData(user.getId());
        System.out.println("[ALL TASKS HAVE BEEN DELETED SUCCESSFULLY]");
    }

    // Удаляет задания всех юзеров
    public void adminExecute() throws Exception {
        System.out.println("[CLEAR DB TASKS]");
        System.out.println("[Enter \"Y\" if you confirm deletion of all tasks]".toUpperCase());
        String confirm = serviceLocator.getTerminalService().nextLine();
        if(!"y".equals(confirm.toLowerCase())){
            System.out.println("[operation has not been confirmed]".toUpperCase());
            return;
        }
        serviceLocator.getTaskService().clearData();
        System.out.println("[ALL TASKS HAVE BEEN DELETED SUCCESSFULLY]");
    }
}
