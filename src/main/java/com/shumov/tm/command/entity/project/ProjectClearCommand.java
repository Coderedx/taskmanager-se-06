package com.shumov.tm.command.entity.project;

import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.User;
import com.shumov.tm.util.constant.UserRoleType;

public class ProjectClearCommand extends AbstractCommand {

    public ProjectClearCommand() {

    }

    @Override
    public String command() {
        return "project-clear";
    }

    @Override
    public String getDescription() {
        return "Remove all projects";
    }

    @Override
    public void execute() throws Exception {
        if(serviceLocator.getCurrentUser().getUserRoleType().equals(UserRoleType.ADMIN)){
            adminExecute();
        } else {
            userExecute();
        }
    }

    public void userExecute() throws Exception {
        System.out.println("[CLEAR PROJECTS]");
        System.out.println("[Enter \"Y\" if you confirm deletion of all projects]".toUpperCase());
        String confirm = serviceLocator.getTerminalService().nextLine();
        if(!"y".equals(confirm.toLowerCase())){
            System.out.println("[operation has not been confirmed]".toUpperCase());
            return;
        }
        User user = serviceLocator.getCurrentUser();
        serviceLocator.getProjectService().clearData(serviceLocator.getTaskService(), user.getId());
        System.out.println("[ALL PROJECTS HAVE BEEN DELETED SUCCESSFULLY]");
    }

    // Удаляет проекты всех юзеров
    // Добавить удаление админу только своих проектов
    public void adminExecute() throws Exception {
        System.out.println("[CLEAR DB PROJECTS]");
        System.out.println("[Enter \"Y\" if you confirm deletion of all projects]".toUpperCase());
        String confirm = serviceLocator.getTerminalService().nextLine();
        if(!"y".equals(confirm.toLowerCase())){
            System.out.println("[operation has not been confirmed]".toUpperCase());
            return;
        }
        serviceLocator.getProjectService().clearData(serviceLocator.getTaskService());
        System.out.println("[ALL PROJECTS HAVE BEEN DELETED SUCCESSFULLY]");
    }
}

