package com.shumov.tm.command.entity.user;

import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.User;
import com.shumov.tm.service.UserService;
import com.shumov.tm.util.constant.UserRoleType;

import java.util.Scanner;

public class UserRegCommand extends AbstractCommand {

    @Override
    public String command() {
        return "user-reg";
    }

    @Override
    public String getDescription() {
        return "New user registration";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[NEW USER REGISTRATION]");
        User currentUser = serviceLocator.getCurrentUser();
        if(currentUser == null) {
           createUser();
        } else if (currentUser.getUserRoleType().equals(UserRoleType.ADMIN)){
           createAdmin();
        }
    }

    public void createUser() throws Exception  {
        UserService userService = serviceLocator.getUserService();
        Scanner terminalService = serviceLocator.getTerminalService();
        System.out.println("[ENTER LOGIN FOR NEW USER]");
        String login = terminalService.nextLine();
        userService.isWrongLogin(login);
        System.out.println("[ENTER PASSWORD FOR NEW USER]");
        String pass = terminalService.nextLine();
        userService.isWrongPass(pass);
        userService.createNewUser(login, pass);
        System.out.println("NEW USER CREATED SUCCESSFULLY");
    }

    public void createAdmin() throws Exception {
        UserService userService = serviceLocator.getUserService();
        Scanner terminalService = serviceLocator.getTerminalService();
        System.out.println("[ENTER LOGIN FOR NEW USER]");
        String login = terminalService.nextLine();
        userService.isWrongLogin(login);
        System.out.println("[ENTER PASSWORD FOR NEW USER]");
        String pass = terminalService.nextLine();
        userService.isWrongPass(pass);
        System.out.println("[ENTER ROLE TYPE FOR NEW USER}");
        System.out.println("\'admin\' or \'user\'");
        String role = terminalService.nextLine().toLowerCase();
        if("admin".equals(role)){
            userService.createNewUser(login, pass, UserRoleType.ADMIN);
            System.out.println("NEW ADMIN CREATED SUCCESSFULLY");
        } else if ("user".equals(role)){
            userService.createNewUser(login, pass);
            System.out.println("NEW USER CREATED SUCCESSFULLY");
        } else {
            System.out.println("WRONG ROLE TYPE!");
        }
    }

    @Override
    public boolean isSecure() {
        User user = serviceLocator.getCurrentUser();
        if(user==null || user.getUserRoleType().equals(UserRoleType.ADMIN)){
            return true;
        } else {
            return false;
        }
    }
}
