package com.shumov.tm.command.entity.project;

import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.User;
import com.shumov.tm.service.ProjectService;
import com.shumov.tm.util.constant.UserRoleType;

public class ProjectEditCommand extends AbstractCommand {

    public ProjectEditCommand() {

    }

    @Override
    public String command() {
        return "project-edit";
    }

    @Override
    public String getDescription() {
        return "Edit project name";
    }

    @Override
    public void execute() throws Exception {
        if(serviceLocator.getCurrentUser().getUserRoleType().equals(UserRoleType.ADMIN)){
            adminExecute();
        } else {
            userExecute();
        }
    }

    public void adminExecute() throws Exception {
        System.out.println("[EDIT PROJECT NAME BY ID]");
        System.out.println("ENTER PROJECT ID:");
        String projectIdEdit = serviceLocator.getTerminalService().nextLine();
        ProjectService projectService = serviceLocator.getProjectService();
        projectService.isWrongProjectId(projectIdEdit);
        System.out.println("ENTER NEW PROJECT NAME:");
        String projectNameEdit = serviceLocator.getTerminalService().nextLine();
        projectService.isWrongProjectName(projectNameEdit);
        projectService.editProjectNameById(projectIdEdit,projectNameEdit);
        System.out.println("PROJECT NAME HAS BEEN CHANGED SUCCESSFULLY");
    }

    public void userExecute() throws Exception {
        System.out.println("[EDIT PROJECT NAME BY ID]");
        System.out.println("ENTER PROJECT ID:");
        String projectIdEdit = serviceLocator.getTerminalService().nextLine();
        ProjectService projectService = serviceLocator.getProjectService();
        projectService.isWrongProjectId(projectIdEdit);
        System.out.println("ENTER NEW PROJECT NAME:");
        String projectNameEdit = serviceLocator.getTerminalService().nextLine();
        projectService.isWrongProjectName(projectNameEdit);
        User user = serviceLocator.getCurrentUser();
        projectService.editProjectNameById(projectIdEdit,projectNameEdit,user.getId());
        System.out.println("PROJECT NAME HAS BEEN CHANGED SUCCESSFULLY");
    }
}
