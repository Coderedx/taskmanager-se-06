package com.shumov.tm.repository;

import com.shumov.tm.entity.User;
import com.shumov.tm.exception.entity.user.UserException;
import com.shumov.tm.exception.entity.user.UserIsAlreadyExistException;
import com.shumov.tm.exception.entity.user.UserListIsEmptyException;
import com.shumov.tm.exception.entity.user.UserNotExistException;
import com.shumov.tm.util.constant.UserRoleType;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class UserRepository {

    private Map<String, User> userMap = new LinkedHashMap<>();

    public UserRepository() {

    }

    public List<User> findAll() throws UserException {
        if(userMap.isEmpty()){
            throw new UserListIsEmptyException();
        } else {
            return new ArrayList<>(userMap.values());
        }
    }

    public User findOne(String id) throws UserException {
        if (userMap.isEmpty()){
            throw new UserListIsEmptyException();
        } else if(!userMap.containsKey(id)){
            throw new UserNotExistException();
        } else {
            return userMap.get(id);
        }
    }

    public User findOne(String login, String password) throws UserException{
        if(userMap.isEmpty()){
            throw new UserListIsEmptyException();
        }
        for (User userDb : userMap.values()){
            if (userDb.getLogin().equals(login) && userDb.getPasswordHash().equals(password)){
                return userDb;
            }
        }
        throw new UserNotExistException();
    }

    public void persist(User user) throws UserException {
        if(userMap.containsKey(user.getId())){
            throw new UserIsAlreadyExistException();
        }
        for(User userDb : userMap.values()){
            if(user.getLogin().equals(userDb.getLogin())){
                throw new UserIsAlreadyExistException();
            }
        }
        userMap.put(user.getId(), user);
    }

    public void merge(String id, User user){
        user.setId(id);
        userMap.put(id, user);
    }

    public void remove(String id) throws UserException {
        if (userMap.isEmpty()){
            throw new UserListIsEmptyException();
        } else if (!userMap.containsKey(id)){
            throw new UserNotExistException();
        } else {
            userMap.remove(id);
        }
    }

    public void removeAll() throws UserException {
        if(userMap.isEmpty()){
            throw new UserListIsEmptyException();
        } else {
            userMap.clear();
        }
    }
}
