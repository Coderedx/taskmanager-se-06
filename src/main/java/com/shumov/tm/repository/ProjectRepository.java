package com.shumov.tm.repository;

import com.shumov.tm.entity.Project;
import com.shumov.tm.exception.entity.project.ProjectException;
import com.shumov.tm.exception.entity.project.ProjectIsAlreadyExistException;
import com.shumov.tm.exception.entity.project.ProjectListIsEmptyException;
import com.shumov.tm.exception.entity.project.ProjectNotExistException;


import java.util.*;

public class ProjectRepository {

    private Map<String, Project> projectMap = new LinkedHashMap<>();

    public ProjectRepository(){

    }

    public List<Project> findAll() throws ProjectException {
        if(projectMap.isEmpty()) {
            throw new ProjectListIsEmptyException();
        } else {
            return new ArrayList<>(projectMap.values());
        }
    }

    public List<Project> findAll(String userId) throws ProjectException {
        List<Project> list = new ArrayList<>();
        for (Project project: findAll()) {
            if(project.getUserId().equals(userId)){
                list.add(project);
            }
        }
        if(list.isEmpty()){
            throw new ProjectListIsEmptyException();
        } else {
            return list;
        }
    }

    public Project findOne(String projectId) throws ProjectException {
        if(projectMap.isEmpty()){
            throw new ProjectListIsEmptyException();
        } else if(!projectMap.containsKey(projectId)){
            throw new ProjectNotExistException();
        } else {
            return projectMap.get(projectId);
        }
    }

    public Project findOne(String projectId, String userId) throws ProjectException {
        Project project = findOne(projectId);
        if (project.getUserId().equals(userId)){
            return project;
        } else {
            throw new ProjectNotExistException();
        }
    }

    public void persist(Project project) throws ProjectException {
        if(projectMap.containsKey(project.getId())){
            throw new ProjectIsAlreadyExistException();
        } else {
            projectMap.put(project.getId(), project);
        }

    }

    public void merge(String projectId, Project project) {
        project.setId(projectId);
        projectMap.put(projectId, project);
    }

    public void remove(String projectId) throws ProjectException {
        if(projectMap.isEmpty()){
            throw new ProjectListIsEmptyException();
        } else if(!projectMap.containsKey(projectId)){
            throw new ProjectNotExistException();
        } else {
            projectMap.remove(projectId);
        }
    }

    public void remove(String projectId, String userId) throws ProjectException {
        findOne(projectId, userId);
        remove(projectId);
    }

    public void removeAll() throws ProjectException {
        if(projectMap.isEmpty()){
            throw new ProjectListIsEmptyException();
        } else {
            projectMap.clear();
        }
    }

    public void removeAll(String userId) throws ProjectException {
        List<Project> list = findAll(userId);
        for (Project project : list) {
            remove(project.getId());
        }
    }

}
