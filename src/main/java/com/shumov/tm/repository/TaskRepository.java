package com.shumov.tm.repository;

import com.shumov.tm.entity.Task;
import com.shumov.tm.exception.entity.task.TaskException;
import com.shumov.tm.exception.entity.task.TaskIsAlreadyExistException;
import com.shumov.tm.exception.entity.task.TaskListIsEmptyException;
import com.shumov.tm.exception.entity.task.TaskNotExistException;


import java.util.*;

public class TaskRepository {

    private Map<String,Task> taskMap = new LinkedHashMap<>();

    public TaskRepository(){

    }

    public List<Task> findAll() throws TaskException {
        if(taskMap.isEmpty()) {
            throw new TaskListIsEmptyException();
        } else {
            return new ArrayList<>(taskMap.values());
        }
    }

    public List<Task> findAll(String userId) throws TaskException {
        List<Task> list = new ArrayList<>();
        for (Task task: findAll()) {
            if(task.getUserId().equals(userId)){
                list.add(task);
            }
        }
        if(list.isEmpty()){
            throw new TaskListIsEmptyException();
        } else {
            return list;
        }
    }

    public Task findOne(String taskId) throws TaskException {
        if(taskMap.isEmpty()){
            throw new TaskListIsEmptyException();
        } else if(!taskMap.containsKey(taskId)){
            throw new TaskNotExistException();
        } else {
            return taskMap.get(taskId);
        }
    }

    public Task findOne(String taskId, String userId) throws TaskException {
        Task task = findOne(taskId);
        if (task.getUserId().equals(userId)){
            return task;
        } else {
            throw new TaskNotExistException();
        }
    }

    public void persist(Task task) throws TaskException {
        if(taskMap.containsKey(task.getId())){
            throw new TaskIsAlreadyExistException();
        } else {
            taskMap.put(task.getId(), task);
        }
    }

    public void merge(String taskId, Task task) {
        task.setId(taskId);
        taskMap.put(taskId, task);
    }

    public void remove(String taskId) throws TaskException {
        if(taskMap.isEmpty()){
            throw new TaskListIsEmptyException();
        } else if (!taskMap.containsKey(taskId)){
            throw new TaskNotExistException();
        } else {
            taskMap.remove(taskId);
        }
    }

    public void remove(String taskId, String userId) throws TaskException {
        findOne(taskId, userId);
        remove(taskId);
    }

    public void removeAll() throws TaskException {
        if (taskMap.isEmpty()){
            throw new TaskListIsEmptyException();
        } else {
            taskMap.clear();
        }
    }

    public void removeAll(String userId) throws TaskException {
        List<Task> list = findAll(userId);
        for (Task task : list) {
            remove(task.getId());
        }
    }
}
