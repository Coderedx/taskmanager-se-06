package com.shumov.tm.service;

import com.shumov.tm.entity.Project;
import com.shumov.tm.entity.Task;
import com.shumov.tm.exception.input.WrongIdException;
import com.shumov.tm.exception.input.WrongInputException;
import com.shumov.tm.exception.input.WrongNameException;
import com.shumov.tm.exception.entity.project.ProjectException;
import com.shumov.tm.exception.entity.task.TaskException;
import com.shumov.tm.repository.ProjectRepository;

import java.util.List;

public class ProjectService {

    private ProjectRepository projectRepository;

    public ProjectService() {

    }

    public ProjectService(ProjectRepository projectRepository){
        this.projectRepository = projectRepository;
    }

    public void createProject(String projectName, String userId) throws ProjectException  {
        Project project = new Project(projectName, userId);
        projectRepository.persist(project);
    }

    public List<Project> getProjectList() throws ProjectException {
        return projectRepository.findAll();
    }

    public List<Project> getProjectList(String userId) throws ProjectException {
        return projectRepository.findAll(userId);
    }

    public Project getProject(String projectId) throws ProjectException {
        return projectRepository.findOne(projectId);
    }

    public Project getProject(String projectId, String userId) throws ProjectException {
        return projectRepository.findOne(projectId, userId);
    }

    public void editProjectNameById(String projectId, String projectName) throws ProjectException {
        Project project = projectRepository.findOne(projectId);
        project.setName(projectName);
        projectRepository.merge(projectId, project);
    }

    public void editProjectNameById(String projectId, String projectName, String userId) throws ProjectException {
        Project project = projectRepository.findOne(projectId, userId);
        project.setName(projectName);
        projectRepository.merge(projectId, project);
    }

    public void removeProjectById(TaskService taskService, String projectId) throws ProjectException, TaskException {
        projectRepository.remove(projectId);
        for (Task task : taskService.getTaskList()) {
            if(projectId.equals(task.getIdProject())){
                taskService.removeTaskById(task.getId());
            }
        }
    }

    public void removeProjectById(TaskService taskService, String projectId, String userId) throws ProjectException,
            TaskException {
        projectRepository.remove(projectId, userId);
        for (Task task : taskService.getTaskList(userId)) {
            if(projectId.equals(task.getIdProject())){
                taskService.removeTaskById(task.getId(), userId);
            }
        }
    }

    public void clearData(TaskService taskService) throws ProjectException, TaskException{
        projectRepository.removeAll();
        taskService.clearData();
    }

    public void clearData(TaskService taskService, String userID) throws ProjectException, TaskException{
        projectRepository.removeAll(userID);
        taskService.clearData(userID);
    }



    public void isWrongProjectName(String projectName) throws WrongInputException{
        if (projectName==null || projectName.isEmpty()){
            throw new WrongNameException();
        }
    }

    public void isWrongProjectId(String projectId) throws WrongInputException{
        if(projectId==null || projectId.isEmpty()){
            throw new WrongIdException();
        }
    }
}
