package com.shumov.tm.service;

import com.shumov.tm.entity.Task;
import com.shumov.tm.exception.input.WrongIdException;
import com.shumov.tm.exception.input.WrongInputException;
import com.shumov.tm.exception.input.WrongNameException;
import com.shumov.tm.exception.entity.task.TaskException;
import com.shumov.tm.repository.TaskRepository;

import java.util.ArrayList;
import java.util.List;

public class TaskService {

    private TaskRepository taskRepository;

    public TaskService() {

    }

    public TaskService(TaskRepository taskRepository){
        this.taskRepository = taskRepository;
    }

    public void createTask(String taskName, String projectId, String userId) throws TaskException {
        taskRepository.persist(new Task(taskName, projectId, userId));
    }

    public List<Task> getTaskList() throws TaskException {
        return taskRepository.findAll();
    }

    public List<Task> getTaskList(String userId) throws TaskException {
        return taskRepository.findAll(userId);
    }

    public Task getTask(String taskId) throws TaskException {
        return taskRepository.findOne(taskId);
    }


    public Task getTask(String taskId, String userId) throws TaskException {
        return taskRepository.findOne(taskId, userId);
    }


    public List<Task> getProjectTasks(String projectId) throws TaskException {
        List<Task> tasks = new ArrayList<>();
        for (Task task : taskRepository.findAll()) {
            if(projectId.equals(task.getIdProject())){
                tasks.add(task);
            }
        }
        return tasks;
    }

    public List<Task> getProjectTasks(String projectId, String userId) throws TaskException {
        List<Task> tasks = new ArrayList<>();
        for (Task task : taskRepository.findAll(userId)) {
            if(projectId.equals(task.getIdProject())){
                tasks.add(task);
            }
        }
        return tasks;
    }

//    public void mergeTask(String taskId, Task task) throws TaskException{
//        taskRepository.merge(taskId,task);
//    }

    public void clearData() throws TaskException {
        taskRepository.removeAll();
    }

    public void clearData(String userID) throws TaskException{
        taskRepository.removeAll(userID);
    }

    public void editTaskNameById(String taskId, String taskName) throws  TaskException {
        Task task = taskRepository.findOne(taskId);
        task.setName(taskName);
        taskRepository.merge(taskId, task);
    }

    public void editTaskNameById(String taskId, String taskName, String userId) throws  TaskException {
        Task task = taskRepository.findOne(taskId, userId);
        task.setName(taskName);
        taskRepository.merge(taskId, task);
    }

    public void removeTaskById(String taskId) throws TaskException{
        taskRepository.remove(taskId);
    }

    public void removeTaskById(String taskId, String userId) throws TaskException{
        taskRepository.remove(taskId, userId);
    }

    public void addTaskInProject(String taskId, Task task) throws TaskException{
        taskRepository.merge(taskId,task);
    }

    public void addTaskInProject(String taskId, Task task, String userId) throws TaskException{
        taskRepository.findOne(taskId,userId);
        taskRepository.merge(taskId,task);
    }

    public void isWrongTaskName(String taskName) throws WrongInputException {
        if (taskName==null || taskName.isEmpty()){
            throw new WrongNameException();
        }
    }

    public void isWrongTaskId(String taskId) throws WrongInputException{
        if(taskId==null || taskId.isEmpty()){
            throw new WrongIdException();
        }
    }
}
