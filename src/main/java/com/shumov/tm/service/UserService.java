package com.shumov.tm.service;

import com.shumov.tm.entity.User;
import com.shumov.tm.exception.entity.user.UserException;
import com.shumov.tm.repository.UserRepository;
import com.shumov.tm.util.constant.UserRoleType;

import java.io.IOException;

public class UserService {

    private UserRepository userRepository;

    public UserService() {

    }

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User getUser(String login, String password) throws UserException {
        return userRepository.findOne(login,password);
    }

    public void mergeUser(User user) throws UserException {
        userRepository.merge(user.getId(), user);
    }

    public void createNewUser(String login, String password) throws UserException {
        User user = new User(login, password);
        userRepository.persist(user);
    }

    public void createNewUser(String login, String password, UserRoleType userRoleType) throws UserException {
        User user = new User(login, password, userRoleType);
        userRepository.persist(user);
    }

    public void isWrongLogin (String login) throws IOException {
        if (login==null || login.isEmpty()){
            throw new IOException("WRONG LOGIN!");
        }
    }

    public void isWrongPass(String pass) throws IOException{
        if(pass==null || pass.isEmpty()){
            throw new IOException("WRONG PASSWORD!");
        }
    }

    public void isWrongDecrip(String description) throws IOException {
        if(description==null || description.isEmpty()){
            throw new IOException("WRONG DESCRIPTION!");
        }
    }
}
