package com.shumov.tm.exception.entity.task;

public class TaskException extends Exception {

    public TaskException() {
    }

    public TaskException(String message) {
        super(message);
    }
}
