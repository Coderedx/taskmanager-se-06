package com.shumov.tm.exception.entity.project;

public class ProjectListIsEmptyException extends ProjectException {

    public ProjectListIsEmptyException() {
        super("PROJECT LIST IS EMPTY!");
    }
}
