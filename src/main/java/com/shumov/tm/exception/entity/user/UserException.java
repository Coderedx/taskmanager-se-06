package com.shumov.tm.exception.entity.user;

public class UserException extends Exception {

    public UserException() {

    }

    public UserException(String message) {
        super(message);
    }
}
