package com.shumov.tm.exception.entity.task;

public class TaskIsAlreadyExistException extends TaskException {

    public TaskIsAlreadyExistException() {
        super("TASK WITH THIS ID IS ALREADY EXIST!");
    }
}
