package com.shumov.tm.exception.entity.user;

public class UserNotExistException extends UserException {

    public UserNotExistException() {
        super("USER WITH THIS LOGIN DOES NOT EXIST OR WRONG PASSWORD!");
    }
}
