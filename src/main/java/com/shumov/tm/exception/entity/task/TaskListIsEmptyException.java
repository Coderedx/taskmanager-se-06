package com.shumov.tm.exception.entity.task;

public class TaskListIsEmptyException extends TaskException {

    public TaskListIsEmptyException(){
        super("TASK LIST IS EMPTY!");
    }
}
