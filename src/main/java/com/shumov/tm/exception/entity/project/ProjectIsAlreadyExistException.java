package com.shumov.tm.exception.entity.project;

public class ProjectIsAlreadyExistException extends ProjectException {

    public ProjectIsAlreadyExistException(){
        super("PROJECT WITH THIS ID IS ALREADY EXIST!");
    }
}
