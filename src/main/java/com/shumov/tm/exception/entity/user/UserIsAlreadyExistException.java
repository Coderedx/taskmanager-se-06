package com.shumov.tm.exception.entity.user;

public class UserIsAlreadyExistException extends UserException {

    public UserIsAlreadyExistException() {
        super("USER WITH THIS ID IS ALREADY EXIST!");
    }
}
