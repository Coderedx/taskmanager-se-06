package com.shumov.tm.exception.entity.task;

public class TaskNotExistException extends TaskException {

    public TaskNotExistException(){
        super("TASK WITH THIS ID DOES NOT EXIST!");
    }
}
