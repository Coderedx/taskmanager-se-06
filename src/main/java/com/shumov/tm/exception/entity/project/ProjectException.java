package com.shumov.tm.exception.entity.project;

public class ProjectException extends Exception {

    public ProjectException() {

    }

    public ProjectException(String message) {
        super(message);
    }

}
