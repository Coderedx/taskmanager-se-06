package com.shumov.tm.exception.entity.user;

public class UserListIsEmptyException extends UserException {

    public UserListIsEmptyException() {
        super("USER LIST IS EMPTY!");
    }
}
